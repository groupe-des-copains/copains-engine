#include "../engine/particles/classic_timer.hpp"
#include "SDL_timer.h"

ClassicTimer::ClassicTimer()
{
    this->start();
}

void ClassicTimer::start()
{
    this->started_at = SDL_GetTicks();
}

uint32 ClassicTimer::read() const
{
    return SDL_GetTicks() - this->started_at;
}

float ClassicTimer::read_sec() const
{
    return float(SDL_GetTicks() - this->started_at) / 1000.0f;
}