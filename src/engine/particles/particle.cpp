#include "../engine/particles/particle.hpp"

Particle::Particle() : life(0)
{
}

void Particle::init(fPoint pos,
                    float start_speed,
                    float end_speed,
                    float angle,
                    double rot_speed,
                    float start_size,
                    float end_size,
                    unsigned int life,
                    Spritesheet *sheet,
                    SDL_Color start_color,
                    SDL_Color end_color,
                    SDL_BlendMode blend_mode,
                    bool vortex_sensitive,
                    std::string particle_texture)
{

    this->sheet = sheet;
    this->state.live.pos = pos;
    this->state.live.start_vel.x = start_speed * cos(DEG_2_RAD(angle));
    this->state.live.start_vel.y = -start_speed * sin(DEG_2_RAD(angle));

    this->state.live.end_vel.x = end_speed * cos(DEG_2_RAD(angle));
    this->state.live.end_vel.y = -end_speed * sin(DEG_2_RAD(angle));

    this->state.live.start_rot_speed = rot_speed;
    this->state.live.current_rot_speed = rot_speed;

    // // Life properties
    this->life = this->state.live.start_life = life;
    this->state.live.current_size = this->state.live.start_size = start_size;
    this->state.live.end_size = end_size;
    this->state.live.t = 0.0f;

    // Color properties
    this->state.live.start_color = start_color;
    this->state.live.end_color = end_color;
    this->state.live.blend_mode = blend_mode;

    this->state.live.rect = this->state.live.rect_size = this->sheet->get_animations(particle_texture)[0];

    // // Vortex
    this->state.live.vortex_sensitive = vortex_sensitive;

    // Add vortex to the system (optional and only one is allowed)
    if (this->state.live.vortex_sensitive)
    {
        // TODO: voir pour configurer le vortex
        this->add_vortex({250.0f, 200.0f}, 10.0f, 30.0f);
    }

    this->sheet->set_blend_mode(this->state.live.blend_mode);

}

bool Particle::is_alive()
{
    return this->life > 0;
}

Particle *Particle::get_next()
{
    return this->state.next;
}

void Particle::set_next(Particle *next)
{
    this->state.next = next;
}

bool Particle::draw(SDL_Renderer *renderer)
{
    bool ret = true;

    int size = (int)this->state.live.start_size;
    SDL_Rect tmpRect = {0, 0, size, size};
    float center_x = this->state.live.pos.x + ((tmpRect.w - this->state.live.rect_size.w) / 2.0f);
    float center_y = this->state.live.pos.y + ((tmpRect.h - this->state.live.rect_size.h) / 2.0f);

    SDL_Rect source_rect = this->state.live.rect;
    SDL_Rect pos_rect = source_rect;
    pos_rect.w = this->state.live.rect_size.w;
    pos_rect.h = this->state.live.rect_size.h;
    pos_rect.x = center_x;
    pos_rect.y = center_y;

    SDL_Color color;
    if (this->state.live.start_life > MIN_LIFE_TO_INTERPOLATE)
    {
        color = rgb_interpolation(this->state.live.start_color, this->state.live.t, this->state.live.end_color);
    }

    this->sheet->set_color_alpha_mode(color);

    auto error = SDL_RenderCopy(renderer, this->sheet->get_texture(), &source_rect, &pos_rect);
    if (error != 0)
    {
        ret = false;
        std::cout << "[probleme render copy particle]" << SDL_GetError() << std::endl;
    }

    // Calculating new rotation according to rotation speed
    this->state.live.current_rot_speed += this->state.live.start_rot_speed;

    // Time step increment to interpolate colors
    this->state.live.t += (1.0f / (float)this->state.live.start_life);

    if (this->state.live.t >= 1.0f)
        this->state.live.t = 0.0f;

    return ret;
}

int Particle::update(float dt)
{

    if (!this->state.live.vortex_sensitive && this->vortex.scale != 0 && this->vortex.speed != 0)
    {
        this->add_vortex({0.0f, 0.0f}, 0.0f, 0.0f);
    }

    // Age ratio is used to interpolate between particle properties
    this->state.live.age_ratio = (float)this->life / (float)this->state.live.start_life;

    // Particle size interpolation
    this->state.live.current_size = interpolate_between_range(this->state.live.start_size, this->state.live.t, this->state.live.end_size);

    // Particle speed interpolation
    this->state.live.current_vel.x = interpolate_between_range(this->state.live.start_vel.x, this->state.live.t, this->state.live.end_vel.x);
    this->state.live.current_vel.y = interpolate_between_range(this->state.live.start_vel.y, this->state.live.t, this->state.live.end_vel.y);

    // Assign new size to particle rect
    this->state.live.rect_size.w = this->state.live.rect_size.h = this->state.live.current_size;

    this->calculate_particle_pos(dt);
    this->life--;
    return 0;
}

float Particle::interpolate_between_range(float min, float time_step, float max)
{
    return min + (max - min) * time_step;
}

void Particle::calculate_particle_pos(float dt)
{
    float dx = this->state.live.pos.x - this->vortex.pos.x;
    float dy = this->state.live.pos.y - this->vortex.pos.y;
    float vx = -dy * this->vortex.speed;
    float vy = dx * this->vortex.speed;

    float factor = 1.0f / (1.0f + (dx * dx + dy * dy) / this->vortex.scale);

    this->state.live.pos.x += (vx - this->state.live.current_vel.x) * factor + this->state.live.current_vel.x * dt;
    this->state.live.pos.y += (vy - this->state.live.current_vel.y) * factor + this->state.live.current_vel.y * dt;
}

void Particle::add_vortex(fPoint pos, float speed, float scale)
{
    this->vortex.pos = pos;
    this->vortex.speed = speed;
    this->vortex.scale = scale;
}

SDL_Color Particle::rgb_interpolation(SDL_Color start_color, float time_step, SDL_Color end_color)
{
    SDL_Color final_color;

    final_color.r = start_color.r + (end_color.r - start_color.r) * time_step;
    final_color.g = start_color.g + (end_color.g - start_color.g) * time_step;
    final_color.b = start_color.b + (end_color.b - start_color.b) * time_step;
    final_color.a = start_color.a + (end_color.a - start_color.a) * time_step;

    return final_color;
}