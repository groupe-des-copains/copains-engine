#include "../engine/particles/particles_pool.hpp"

ParticlesPool::ParticlesPool(Emitter *emitter)
{
    // Fill the pool according to poolSize needed for the emitter
    // (void)emitter;
    this->pool_size = emitter->get_pool_size();

    this->particle_array = new Particle[this->pool_size];

    // // The first particle is available
    this->first_available = &this->particle_array[0];

    // // Each particle points to the next one
    for (int i = 0; i < this->pool_size - 1; i++)
    {
        this->particle_array[i].set_next(&this->particle_array[i + 1]);
    }

    // // The last particle points to nullptr indicating the end of the vector
    this->particle_array[this->pool_size - 1].set_next(nullptr);
}

ParticlesPool::~ParticlesPool()
{
    std::cout << "[DESTRUCTEUR PARTICLES POOL]" << std::endl;
    delete[] this->particle_array;
    this->particle_array = nullptr;
}

int ParticlesPool::generate(fPoint pos,
                            float start_speed,
                            float end_speed,
                            float angle,
                            double rot_speed,
                            float start_size,
                            float end_size,
                            unsigned int life,
                            Spritesheet *sheet,
                            SDL_Color start_color,
                            SDL_Color end_color,
                            SDL_BlendMode blend_mode,
                            bool vortex_sensitive,
                            std::string particle_texture)
{

    // Check if the pool is not full
    assert(this->first_available != nullptr);
    // Remove it from the available list
    Particle *new_particle = this->first_available;
    this->first_available = new_particle->get_next();

    // Initialize new alive particle
    new_particle->init(pos,
                       start_speed,
                       end_speed,
                       angle,
                       rot_speed,
                       start_size,
                       end_size,
                       life,
                       sheet,
                       start_color,
                       end_color,
                       blend_mode,
                       vortex_sensitive,
                       particle_texture);
                       
    return 0;
}

ParticleState ParticlesPool::update(float dt, SDL_Renderer *renderer)
{    
    ParticleState ret_state = ParticleState::PARTICLE_STATE_NOT_DEF;
    for (int i = 0; i < this->pool_size; i++)
    {
        if (this->particle_array[i].is_alive())
        {
            this->particle_array[i].update(dt);
            if (this->particle_array[i].draw(renderer))
                ret_state = ParticleState::PARTICLE_ALIVE_DRAWN;
            else
                ret_state = ParticleState::PARTICLE_ALIVE_NOT_DRAWN;
        }
        else // if a particle dies it becomes the first available in the pool
        {
            // Add this particle to the front of the vector

            this->particle_array[i].set_next(this->first_available);
            this->first_available = &this->particle_array[i];
            ret_state = ParticleState::PARTICLE_DEAD;
        }
    }
    return ret_state;
}
