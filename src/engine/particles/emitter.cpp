#include <json/value.h>
#include <json/json.h>

#include "../engine/particles/emitter.hpp"
#include <time.h>

Emitter::Emitter(float pos_x, float pos_y, Json::Value &emitter_data, Spritesheet *sheet)
{

    // srand(time(NULL));

    this->sheet = sheet;
    auto e = emitter_data["emitter"];
    this->particle_texture = e["texture_name"].asString();

    this->emit_number = e["emit_number"]["value"].asUInt();
    this->emit_variance = e["emit_variance"]["value"].asUInt();
    this->max_particle_life = e["max_particle_life"]["value"].asUInt();

    this->start_speed = e["start_speed"]["value"].asUInt();
    this->end_speed = e["end_speed"]["value"].asUInt();

    this->start_size = e["start_size"]["value"].asUInt();
    this->end_size = e["end_size"]["value"].asUInt();

    this->rot_speed = e["rot_speed"]["value"].asDouble();
    this->time_step = 1.0f / (float)this->max_particle_life;

    this->life_time = e["lifetime"]["value"].asDouble();
    std::cout << "[lifetime in emitter constructor]" << this->life_time << std::endl;

    this->active = true;

    this->angle_range = this->get_range(e["angle_range"]);
    this->start_speed_rand = this->get_range(e["start_speed"]);
    this->end_speed_rand = this->get_range(e["end_speed"]);
    this->emit_variance_rand = this->get_range(e["emit_variance"]);
    this->life_rand = this->get_range(e["life"]);
    this->start_size_rand = this->get_range(e["start_size"]);

    // Pool size calculations
    this->max_particles_per_frame = this->emit_number + this->emit_variance;
    this->pool_size = this->max_particles_per_frame * (this->max_particle_life + 1);
    this->vortex_sensitive = e["vortex_sensitive"]["value"].asBool();

    this->pos = {pos_x, pos_y};

    std::string blend_mode = e["blend_mode"]["mode"].asString();

    if (blend_mode == "add")
        this->blend_mode = SDL_BlendMode::SDL_BLENDMODE_ADD;
    else if (blend_mode == "blend")
        this->blend_mode = SDL_BlendMode::SDL_BLENDMODE_BLEND;
    else if (blend_mode == "mod")
        this->blend_mode = SDL_BlendMode::SDL_BLENDMODE_MOD;
    else if (blend_mode == "none")
        this->blend_mode = SDL_BlendMode::SDL_BLENDMODE_NONE;

    this->my_pool = new ParticlesPool(this);

    this->init_colors(e["start_color"], e["end_color"], e["color_variance"]);

    if (this->life_time != -1.0f && this->life_time > 0.0f)
    {
        this->life_timer.start();
    }
}

Emitter::~Emitter()
{
    std::cout << "[destructor emitter]" << std::endl;
    delete this->my_pool;
    this->my_pool = nullptr;
}

unsigned int Emitter::get_pool_size()
{
    return this->pool_size;
}

float Emitter::range_random_num(Emitter::FloatRange range)
{
    float random = ((float)rand()) / (float)RAND_MAX;
    float diff = range.max - range.min;
    float r = random * diff;

    return range.min + r;
}

void Emitter::start_emission(double timer)
{
    if (!this->active)
    {
        this->active = true;
        this->emission_time = timer;
        this->emission_timer.start();
    }
}

void Emitter::stop_emission(double timer)
{
    if (this->active)
    {
        this->active = false;
        this->stop_time = timer;
        this->stop_timer.start();
    }
}

void Emitter::move_emitter(fPoint new_pos)
{
    this->pos = new_pos;
}

fPoint Emitter::get_emitter_pos() const
{
    return pos;
}

void Emitter::set_vortex_sensitive(bool sensitive)
{
    this->vortex_sensitive = sensitive;
}

void Emitter::update()
{
    if (this->active)
    {
        // Particle generation from pool
        auto variance = this->range_random_num(this->emit_variance_rand);
        this->emission_rate = (int)(this->emit_number + this->emit_variance * variance);

        for (unsigned int i = 1; i <= this->emission_rate; i++)
        {
            float tmp_start_speed = this->start_speed * this->range_random_num(this->start_speed_rand);
            float tmp_end_speed = this->end_speed * this->range_random_num(this->end_speed_rand);
            float rand_angle = this->range_random_num(this->angle_range);
            float rand_start = this->start_size * this->range_random_num(this->start_size_rand);
            float rand_end = this->start_size * this->range_random_num(this->start_size_rand);

            float start_size = this->range_random_num({rand_start, rand_end});
            double rand_rot_speed = this->rot_speed * this->range_random_num(this->rot_speed_rand);
            this->my_pool->generate(this->pos,
                                    tmp_start_speed,
                                    tmp_end_speed,
                                    rand_angle,
                                    rand_rot_speed,
                                    start_size,
                                    this->end_size,
                                    this->max_particle_life,
                                    this->sheet,
                                    this->start_color,
                                    this->end_color,
                                    this->blend_mode,
                                    this->vortex_sensitive,
                                    this->particle_texture);
            this->time_step += this->time_step;
        }
    }

    // Emission timing calculations
    if (this->stop_time > 0.0f && !this->active)
    {
        this->emission_time = 0.0f;
        if (this->stop_timer.read_ms() >= this->stop_time)
        {
            this->active = true;
            this->stop_time = 0.0f;
        }
    }

    if (this->emission_time > 0.0f)
    {
        this->stop_time = 0.0f;
        if (this->emission_timer.read_ms() >= this->emission_time)
        {
            this->active = false;
            this->emission_time = 0.0f;
        }
    }

    if (this->life_time > 0.0f)
    {
        if (this->life_timer.read_ms() >= this->life_time)
        {
            this->active = false;
            this->life_time = 0.0f;
        }
    }
}

int Emitter::get_color_rand(int color, int variance)
{
    FloatRange range = {
        (float)color - variance,
        (float)color + variance};
    int ret = int(this->range_random_num(range));
    if (ret > 255)
        ret = 255;
    else if (ret < 0)
        ret = 0;

    return ret;
}

void Emitter::init_colors(Json::Value start_color, Json::Value end_color, Json::Value color_variance)
{
    this->start_color.r = this->get_color_rand(start_color["r"].asInt(), color_variance["r"].asInt());
    this->start_color.g = this->get_color_rand(start_color["g"].asInt(), color_variance["g"].asInt());
    this->start_color.b = this->get_color_rand(start_color["b"].asInt(), color_variance["b"].asInt());
    this->start_color.a = this->get_color_rand(start_color["a"].asInt(), color_variance["a"].asInt());

    this->end_color.r = this->get_color_rand(end_color["r"].asInt(), color_variance["r"].asInt());
    this->end_color.g = this->get_color_rand(end_color["g"].asInt(), color_variance["g"].asInt());
    this->end_color.b = this->get_color_rand(end_color["b"].asInt(), color_variance["b"].asInt());
    this->end_color.a = this->get_color_rand(end_color["a"].asInt(), color_variance["a"].asInt());
}

bool Emitter::draw(float dt, SDL_Renderer *renderer)
{
    bool ret = true;
    // Updating particles in the pool
    /* NOTE: if lifetime is 0 and last particles have been updated
	then the emitter is automatically destroyed */
    if (this->my_pool->update(dt, renderer) == ParticleState::PARTICLE_DEAD && this->life_time == 0.0f)
    {
        this->to_destroy = true;
    }
    else if (this->my_pool->update(dt, renderer) == ParticleState::PARTICLE_ALIVE_NOT_DRAWN)
    {
        std::cout << this->angle_range.max << std::endl;
        ret = false;
    }

    return ret;
}

Emitter::FloatRange Emitter::get_range(Json::Value data_to_transform)
{
    FloatRange ret = {0, 0};
    ret.min = data_to_transform["min"].asFloat();
    ret.max = data_to_transform["max"].asFloat();
    return ret;
}

bool Emitter::have_to_destroy()
{
    return this->to_destroy;
}
