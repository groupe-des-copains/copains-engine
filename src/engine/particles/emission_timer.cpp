#include "../engine/particles/emission_timer.hpp"
#include "SDL_timer.h"

uint64 EmissionTimer::frequency = 0;

EmissionTimer::EmissionTimer()
{
	if (this->frequency == 0)
	{
		this->frequency = SDL_GetPerformanceFrequency();
	}

	this->start();
}

void EmissionTimer::start()
{
	this->started_at = SDL_GetPerformanceCounter();
}

double EmissionTimer::read_ms() const
{
	auto time = double(SDL_GetPerformanceCounter() - this->started_at);
	return (time / double(this->frequency)) * 1000;
}

unsigned int EmissionTimer::read_ticks() const
{
	return SDL_GetPerformanceCounter() - this->started_at;
}