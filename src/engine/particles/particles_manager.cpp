#include "../engine/particles/particles_manager.hpp"

ParticlesManager::ParticlesManager(DataManager *data_manager)
{
    std::cout << "[init particles_manager]" << std::endl;
    this->data_manager = data_manager;
}

int ParticlesManager::create_emitter(iPoint pos, std::string particles_effect, std::string name)
{
    std::cout << "[create emitter]" << std::endl;
    if (this->emitters.find(name) == this->emitters.end())
    {   
        auto emitter = new Emitter(pos.x, pos.y, this->data_manager->emitters[particles_effect], this->data_manager->decorations["particles"]);
        emitter->start_emission(5000.0f);
        this->emitters.insert({name, emitter});
        return 0;
    }
    return -1;
}  

int ParticlesManager::update_emitters(SDL_Renderer *renderer, float dt)
{
    std::map<std::string, int>::iterator it;
    std::vector<std::string> to_delete{};

    for (auto const &p : this->emitters)
    {
        if (p.second->have_to_destroy())
        {
            to_delete.push_back(p.first);
            delete (p.second);
        }
        else
        {
            p.second->update();
            p.second->draw(dt, renderer);
        }
    }

    for (auto emitter : to_delete)
    {
        
        this->emitters.erase(emitter);
    }

    return 0;
}
