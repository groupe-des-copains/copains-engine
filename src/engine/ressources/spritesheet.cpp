// #pragma once
#include "../engine/ressources/spritesheet.hpp"

// TODO: Voir une conversion en type pour la config ?

Spritesheet::Spritesheet(Json::Value config, Image sheet_img)
{
    std::cout << config["filename"];
    this->config = config;
    this->sheet_img = sheet_img;
    this->load();
}

int Spritesheet::load()
{
    if (this->config["format"] == "precis")
    {
        return this->load_particular();
    }

    return this->load_simple();
}

int Spritesheet::add_effect(Image sheet_img, std::string key)
{
    this->effects.insert({key, sheet_img});
    return 0;
}

int Spritesheet::load_particular()
{
    // Pareil là à voir un type, format
    unsigned x = 0, y = 0;
    auto len = this->config["animations"].size();

    for (unsigned int i = 0; i < len; i++)
    {
        std::string name_anim = this->config["animations"][i]["name"].asString();
        unsigned int start_x = this->config["animations"][i]["start_x"].asUInt();
        unsigned int start_y = this->config["animations"][i]["start_y"].asUInt();
        unsigned int width = this->config["animations"][i]["width"].asUInt();
        unsigned int height = this->config["animations"][i]["height"].asUInt();
        unsigned int nb_img_h = this->config["animations"][i]["nb_img_h"].asUInt();
        unsigned int nb_img_v = this->config["animations"][i]["nb_img_v"].asUInt();
        unsigned int skip_x = this->config["animations"][i]["skip_x"].asUInt();
        unsigned int skip_y = this->config["animations"][i]["skip_y"].asUInt();

        this->etats.push_back(name_anim);

        std::vector<SDL_Rect> rects;
        SDL_Rect to_add_rect;

        for (unsigned int v = 0; v < nb_img_v; v++)
        {
            for (unsigned int h = 0; h < nb_img_h; h++)
            {

                x = start_x + h * (skip_x + width);
                y = start_y + v * (skip_y + height);

                to_add_rect.x = x;
                to_add_rect.y = y;
                to_add_rect.w = width;
                to_add_rect.h = height;
                rects.push_back(to_add_rect);
            }
        }
        this->animations.insert({name_anim, rects});
    }
    return 0;
}

int Spritesheet::load_simple()
{
    auto len = this->config["animations"].size();

    unsigned int x = 0, y = 0;
    unsigned int width = this->config["img_width"].asUInt();
    unsigned int height = this->config["img_height"].asUInt();
    unsigned int nb_img_h = this->config["nb_img_h"].asUInt();
    unsigned int nb_img_v = this->config["nb_img_v"].asUInt();

    for (unsigned int i = 0; i < len; i++)
    {

        auto anim = this->config["animations"][i];
        std::string name_anim = anim["name"].asString();
        this->etats.push_back(name_anim);

        (void)nb_img_v;
        // anim["animation_speed"];

        if (name_anim == "skip")
        {
            x += anim["nb_frames"].asUInt();
            if (x >= nb_img_h)
            {
                y += x / nb_img_h;
                x = x % nb_img_h;
                continue;
            }
        }
        std::vector<SDL_Rect> rects;
        for (unsigned int k = 0; k < anim["nb_frames"].asUInt(); k++)
        {
            SDL_Rect rect_to_add;
            rect_to_add.x = x * width;
            rect_to_add.x = x * width;
            rect_to_add.y = y * height;
            rect_to_add.w = width;
            rect_to_add.h = height;

            x++;
            if (x >= nb_img_h)
            {
                x = 0;
                y++;
            }
            rects.push_back(rect_to_add);
        }

        if (this->animations.count(name_anim) == 0)
        {
            this->animations.insert({name_anim, rects});
        }
    }

    return 0;
}

int Spritesheet::set_color_alpha_mode(SDL_Color color)
{
    SDL_SetTextureColorMod(this->sheet_img.texture, color.r, color.g, color.b);
    SDL_SetTextureAlphaMod(this->sheet_img.texture, color.a);
    return 0;
}

int Spritesheet::set_blend_mode(SDL_BlendMode blend_mode)
{
    SDL_SetTextureBlendMode(this->sheet_img.texture, blend_mode);
    return 0;
}

std::vector<SDL_Rect> Spritesheet::get_animations(std::string texture)
{
    return this->animations[texture];
}

SDL_Texture *Spritesheet::get_texture()
{
    return this->sheet_img.texture;
}

SDL_Texture *Spritesheet::get_texture_effect(std::string effect_name)
{
    if (this->effects.count(effect_name))
    {
        return this->effects[effect_name].texture;
    }

    return this->sheet_img.texture;
}

std::string Spritesheet::get_first_etat()
{
    return this->etats[0];
}

float Spritesheet::get_scale()
{
    return this->config["scale"].asFloat();
}
