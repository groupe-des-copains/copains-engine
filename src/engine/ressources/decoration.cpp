#include <iostream>
#include "../engine/ressources/decoration.hpp"

Decoration::Decoration(Spritesheet *sheet, std::string etat, int x, int y, bool flip_horizontal, bool flip_vertical, int rotation, float scale) : Sprite(sheet)
{
    (void)sheet;
    std::cout << "[create Decoration]" << std::endl;
    this->pos_x = x;
    this->pos_y = y;
    this->rotation = rotation;
    this->scale_into_layer = scale;

    if (etat != "none")
    {
        this->etat = etat;
    }
    
    if (flip_vertical && flip_horizontal)
    {
        this->flip = (SDL_RendererFlip)(SDL_FLIP_HORIZONTAL | SDL_FLIP_VERTICAL);
    }
    else if (flip_horizontal)
    {
        this->flip = SDL_FLIP_HORIZONTAL;
    }
    else if (flip_vertical)
    {
        this->flip = SDL_FLIP_VERTICAL;
    }
    else
    {
        this->flip = SDL_FLIP_NONE;
    }
}

void Decoration::set_anim(unsigned int start_anim)
{
    this->tick_anim = start_anim;
}


float Decoration::get_scale()
{
    return this->scale * this->scale_into_layer;
}

SDL_RendererFlip Decoration::get_flip()
{
    return this->flip;
}

int Decoration::get_rotation()
{
    return this->rotation;
}