#include <iostream>
#include "../engine/ressources/sprite.hpp"

Sprite::Sprite(Spritesheet *sheet)
{
    this->sheet = sheet;
    this->etat = this->sheet->get_first_etat();
    this->tick_anim = 0;
    if (this->sheet->get_scale())
    {
        this->scale = this->sheet->get_scale();
    }
    else
    {
        this->scale = 1;
    }
    this->rect = this->sheet->get_animations(this->etat)[0];
}

int Sprite::animate(Uint32 ticks)
{
    Uint32 sprite = (ticks / 100) % (this->sheet->get_animations(this->etat).size());
    // this->tick_anim++;
    // if (this->tick_anim == this->sheet->etats.size())
    // {
    this->tick_anim = sprite;
    // }
    return 0;
}

float Sprite::get_scale()
{
    return this->scale;
}

SDL_Rect Sprite::get_rect()
{
    return this->sheet->get_animations(this->etat)[this->tick_anim];
}

SDL_Texture *Sprite::get_texture(std::string effect)
{
    if (effect.size())
    {
        return this->sheet->get_texture_effect(effect);
    }
    return this->sheet->get_texture();
}

int Sprite::get_pos_x()
{
    return this->pos_x;
}

int Sprite::get_pos_y()
{
    return this->pos_y;
}
