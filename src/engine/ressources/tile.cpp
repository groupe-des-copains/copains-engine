#include <iostream>
#include "../engine/ressources/tile.hpp"

Tile::Tile(Spritesheet *sheet, int x, int y, std::string etat) : Solid(sheet)
{
    this->etat = etat;
    this->pos_x = x * this->get_rect().w;
    this->pos_y = y * this->get_rect().h;
}