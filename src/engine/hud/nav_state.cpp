#include "../../../include/engine/hud/nav_state.hpp"

#include <iostream>

NavState::NavState(
    // DataManager data_manager,
    // screen_resolution screen_res,
    States name) : State()
{
    // (void)data_manager;
    // (void)screen_res;
    (void)name;
    this->alpha_bg = {0, 0, 0, 0};

    this->text_color = {255, 255, 255, 0};

    if (TTF_Init() < 0)
    {
        std::cout << "TTF_Init: " << TTF_GetError() << std::endl;
    }

    TTF_Font *font_rockndut = TTF_OpenFont("data/assets/fonts/rockndut.ttf", 25);
    this->font = font_rockndut;
    if (!font_rockndut)
    {
        std::cout << "Impossible de charger la font : " << TTF_GetError() << std::endl;
    }

}

NavState::~NavState(void)
{
    TTF_CloseFont(this->font);
    TTF_Quit();
}

void NavState::draw_bg(SDL_Renderer *renderer, SDL_Color alpha_bg)
{
    SDL_SetRenderDrawColor(renderer, alpha_bg.r, alpha_bg.g, alpha_bg.b, alpha_bg.a);
}

int NavState::init_texts()
{
    Text text_to_append = {"NavState", this->font, this->text_color, TextPosition::CENTER, TextPosition::MID, 0, 0};
    this->texts.push_back(text_to_append);
    return 0;
}

SDL_Point NavState::get_texture_size(SDL_Texture *texture)
{
    SDL_Point size;
    SDL_QueryTexture(texture, NULL, NULL, &size.x, &size.y);
    return size;
}

void NavState::draw_text(SDL_Renderer *renderer, Text text, screen_resolution screen_res)
{
    (void)screen_res;

    SDL_Surface *text_surface = TTF_RenderText_Blended(text.font, text.text, text.color);
    SDL_Texture *text_texture = SDL_CreateTextureFromSurface(renderer, text_surface);

    SDL_Point texture_size = get_texture_size(text_texture);

    Text last_text = this->texts[this->texts.size() - 1];
    SDL_Surface *last_text_surface = TTF_RenderText_Blended(text.font, last_text.text, text.color);
    SDL_Texture *last_text_texture = SDL_CreateTextureFromSurface(renderer, last_text_surface);
    SDL_Point last_text_texture_size = get_texture_size(last_text_texture);

    int x_rect = 0;
    int y_rect = 0;

    switch (text.x)
    {
    case TextPosition::CENTER:
        x_rect = (screen_res.width / 2) - (texture_size.x / 2);
        break;
    case TextPosition::LEFT:
        x_rect = texture_size.x / 2;
        break;
    case TextPosition::RIGHT:
        x_rect = screen_res.width - (texture_size.x * 2.5);
        break;
    case TextPosition::NEXT:
        x_rect = last_text_texture_size.x + text.custom_x;
        break;
    case TextPosition::NONE:
        x_rect = 0;
        break;
    case TextPosition::CUSTOM:
        x_rect = text.custom_x;
        break;
    default:
        break;
    }

    switch (text.y)
    {
    case TextPosition::TOP:
        y_rect = texture_size.y / 2;
        break;
    case TextPosition::MID:
        y_rect = (screen_res.height - texture_size.y) / 2;
        break;
    case TextPosition::BOTTOM:
        y_rect = (screen_res.height - (texture_size.y) * 2);
        break;
    case TextPosition::NEXT:
        y_rect = last_text_texture_size.y + text.custom_y;
        break;
    case TextPosition::NONE:
        y_rect = 0;
        break;
    case TextPosition::CUSTOM:
        y_rect = text.custom_y;
        break;
    default:
        break;
    }

    SDL_Rect message_rect = {x_rect, y_rect, texture_size.x, texture_size.y};

    SDL_RenderCopy(renderer, text_texture, NULL, &message_rect);

    SDL_FreeSurface(text_surface);
    SDL_DestroyTexture(text_texture);
}

int NavState::draw(SDL_Renderer *renderer, screen_resolution screen_res)
{
    this->draw_bg(renderer, this->alpha_bg);

    for (auto text : this->texts)
    {
        this->draw_text(renderer, text, screen_res);
    }

    return 0;
}
