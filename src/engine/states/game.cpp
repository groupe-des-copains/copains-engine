#include "states/game.hpp"

Game::Game(DataManager *data_manager) : State()
{
    std::cout << "Create state Game" << std::endl;

    this->data_manager = data_manager;
    this->level_manager = new LevelManager(data_manager);
    this->world = new World(data_manager, this->level_manager);
    this->world->particles_manager = new ParticlesManager(data_manager);
    this->gen_entities();
}

int Game::manage_event(std::set<SDL_Keycode> e)
{
    this->manage_key(e);
    return 0;
}

int Game::gen_entities()
{
    this->player = new Player(this->data_manager->sprites["hero"]);
    this->world->add_entity(this->player);
    for (auto solid : this->level_manager->solids)
    {
        this->world->add_solid(solid);
    }
    for (auto layer : this->level_manager->get_layers())
    {
        this->world->add_layer(layer.id, layer);
    }

    return 0;
}

int Game::manage_key(std::set<SDL_Keycode> keys)
{
    if (keys.count(SDLK_ESCAPE))
    {
        State::set_state(States::END);
    }
    else if (keys.count(SDLK_a))
    {
        this->world->particles_manager->create_emitter({250, 150}, "burst", "burst1");
        this->world->particles_manager->create_emitter({500, 750}, "burst", "burst2");
        this->world->particles_manager->create_emitter({500, 750}, "burst", "burst5");
        this->world->particles_manager->create_emitter({250, 450}, "burst", "burst2");
        this->world->particles_manager->create_emitter({350, 800}, "burst", "burst4");
        this->world->particles_manager->create_emitter({450, 800}, "burst", "burst5");
        this->world->particles_manager->create_emitter({550, 1100}, "burst", "burst6");
        this->world->particles_manager->create_emitter({750, 300}, "burst", "burst7");
        this->world->particles_manager->create_emitter({650, 700}, "burst", "burst8");
    }
    else if (keys.count(SDLK_z))
    {
        this->world->particles_manager->create_emitter({150, 50}, "spark", "spark1");
    }
    else if (keys.count(SDLK_e))
    {
        this->world->particles_manager->create_emitter({300, 300}, "bubbles", "bubbles1");
    }
    else if (keys.count(SDLK_r))
    {
        this->world->particles_manager->create_emitter({400, 200}, "fire", "fire1");
        this->world->particles_manager->create_emitter({600, 200}, "fire", "fire2");
    }
    else if (keys.count(SDLK_p))
    {
        State::set_state(States::PAUSE);
    }
    else
    {
        this->world->manage_event(keys);
    }

    return 0;
}

int Game::draw(SDL_Renderer *renderer, screen_resolution screen_res)
{
    (void)screen_res;
    this->world->draw(renderer);
    return 0;
}