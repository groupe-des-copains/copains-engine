#include "states/nav.hpp"

Nav::Nav() : NavState(States::NAV)
{
    this->text_color = {255, 255, 255, 0};
    this->alpha_bg = {65, 70, 220, 0};
    this->init_texts();
}

int Nav::manage_event(std::set<SDL_Keycode> keys)
{
    (void)keys;

    return 0;
}

int Nav::manage_key(std::set<SDL_Keycode> keys)
{
    if (keys.count(SDLK_RETURN))
    {
        State::set_state(States::GAME);
    };
   
    return 0;
}

int Nav::init_texts()
{
    Text text_to_append = {"Copains-Engine", this->font, this->text_color, TextPosition::CENTER, TextPosition::MID, 0, 0};
    Text another_text = {"Play", this->font, this->text_color, TextPosition::CENTER, TextPosition::NEXT, 0, 300};

    this->texts.push_back(text_to_append);
    this->texts.push_back(another_text);
    return 0;
}

int Nav::draw(SDL_Renderer *renderer, screen_resolution screen_res)
{
    this->draw_bg(renderer, this->alpha_bg);

    for (auto text : this->texts)
    {
        this->draw_text(renderer, text, screen_res);
    }

    return 0;
}