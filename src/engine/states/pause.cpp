#include "states/pause.hpp"

Pause::Pause() : NavState(States::PAUSE)
{
    this->text_color = {255, 255, 255, 0};
    this->alpha_bg = {65, 170, 220, 127};
    this->init_texts();
}

int Pause::manage_event(std::set<SDL_Keycode> keys)
{
    (void)keys;

    return 0;
}

int Pause::manage_key(std::set<SDL_Keycode> keys)
{   
    if (keys.count(SDLK_RETURN))
    {
        State::set_state(States::GAME);
    }
    
    return 0;
}

int Pause::init_texts()
{
    Text text_to_append = {"Pause", this->font, this->text_color, TextPosition::CENTER, TextPosition::MID, 0, 0};
    this->texts.push_back(text_to_append);
    return 0;
}

int Pause::draw(SDL_Renderer *renderer, screen_resolution screen_res)
{
    this->draw_bg(renderer, this->alpha_bg);

    for (auto text : this->texts)
    {
        this->draw_text(renderer, text, screen_res);
    }

    return 0;
}