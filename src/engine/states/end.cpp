#include "states/end.hpp"

// End::End() : State()
// {
//     std::cout << "Create state end" << std::endl;
// }

int End::manage_event(SDL_Event e)
{
    // std::cout << "[manage_event end]" << std::endl;
    // std::cout << e.type << std::endl;

    switch (e.type)
    {
    // case SDL_KEYUP:
    //     std::cout << "[keyup]" << std::endl;
    //     break;
    case SDL_KEYDOWN:

        this->manage_key(e);
        break;
    }

    return 0;
}

int End::manage_key(SDL_Event e)
{
    switch (e.key.keysym.sym) // a voir pour ce type
    {
    case SDLK_RIGHT:
        State::set_state(States::NAV);
        break;
    }
    return 0;
}

int End::draw(SDL_Renderer *renderer, screen_resolution screen_res)
{
    SDL_SetRenderDrawColor(renderer, 225, 70, 20, 0.3);
    (void)screen_res;

    return 0;
}