#include "../engine/physics/gravite.hpp"
#include <iostream>

int Gravity(struct VIT *vit, struct POS *pos)
{

    float g = -9;
    float delta_y;
    float vitesse;

    // std::cout << pos->pos_y<< "   pos_y" << std::endl;
    // std::cout << vit->dy<< "   dy" << std::endl;

    delta_y = (1.0 / 2.0) * g - vit->dy;

    if (vit->dy == 0)
    { // a remplacer if collision
        delta_y = 0;
    }

    vitesse = g + vit->dy;
    vit->dy = vitesse;

    // std::cout << delta_y << "  delta_y " << std::endl;
    // std::cout << v0 << "  v0 " << std::endl;

    pos->pos_y += delta_y;

    // std::cout << *pos_y << "   " << delta_y << std::endl;
    return 0;
}
