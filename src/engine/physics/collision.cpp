#include "../engine/physics/collision.hpp"

int Get_Position(struct VIT *vit, struct POS *pos, INE *ine)
{

    Gravity(vit, pos);
    Inertie(vit, pos, ine);

    if (pos->pos_y >= 480) // check_collision 660 le plus bas 480
    {
        pos->pos_y = 480;
        vit->dy = 0;
    };

    std::this_thread::sleep_for(std::chrono::milliseconds(25));
    return 0;
}
