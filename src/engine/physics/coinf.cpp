#include "../engine/physics/coinf.hpp"

Coinf::Coinf(float pos_x, float pos_y)
{
    this->pos = new POS;
    this->vit = new VIT;
    this->ine = new INE;

    this->vit->dx = 0;
    this->vit->dy = -1;
    this->vit->max_dx = 20;
    this->pos->pos_x = pos_x;
    this->pos->pos_y = pos_y;

    this->ine->frottement = 0.5;
    this->running = false;
}

// std::tuple<float,*float> Coinf::pos(void)
// {
//     auto bar = std::make_tuple (this->pos_x,*this->pos_y);
//     return bar;
// }

// std::tuple<float,*float> Coinf::speed(void)
// {
//     auto bar = std::make_tuple (this->dx,*this->dy);
//     return bar;
// }

int Coinf::move()
{
    Get_Position(this->vit, this->pos, this->ine);
    return 0;
}