#include "../engine/physics/inertie.hpp"
#include <iostream>

int Inertie(struct VIT *vit, struct POS *pos, INE *ine)
{
    float g;
    float delta_x;
    float vitesse;
    float sens;

    if (vit->dx > 0)
    {
        sens = 1.0;
    }
    else if (vit->dx < 0)
    {
        sens = -1.0;
    }
    else
    {
        sens = 0;
    };

    g = ine->frottement;

    if ((vit->dx > 0 && vit->dx < 0.01) || (vit->dx < 0 && vit->dx > -0.01))
    {
        vit->dx = 0;
        g = 0;
    }

    delta_x = (-g + abs(vit->dx));

    if (vit->dx == 0)
    {
        delta_x = 0;
        g = 0;
    }

    vitesse = abs(delta_x);

    if (vitesse < 0.01)
    {
        vitesse = 0;
    }

    vit->dx = vitesse * sens;

    if (abs(vit->dx) > vit->max_dx)
    {
        vit->dx = vit->max_dx * sens;
    }

    // std::cout << pos->pos_x<< "   pos_x" << std::endl;
    // std::cout << vit->dx<< "   dx" << std::endl;
    // std::cout << delta_x << "  delta_x " << std::endl;

    pos->pos_x += delta_x * sens;

    return 0;
}
