#include "data_manager.hpp"

std::string remove_extension(std::string *fullname);
int load_image(std::string path, SDL_Surface *surface_to_use);

DataManager::DataManager(ConfigData config, SDL_Renderer *renderer, SDL_Surface *window_surface)
{
    std::cout << "[init data_manager]" << std::endl;
    this->config = config;
    std::vector<std::string> bgs_to_load{"bg.bmp"};
    std::vector<std::string> decorations_to_load{"plant_one.png", "plant_two.png", "plant_three.png", "plant_five.png", "blue_flower_two.png", "blue_flower_one.png", "plants_decoration.png", "plant_wind.png", "background_decoration.png", "floating_platforms.png", "floating_decoration.png", "objects_decoration.png", "particles.png"};
    std::vector<std::string> sprites_to_load{"hero.png"};
    std::vector<std::string> tilesets_to_load{"tileset.png"};
    std::vector<std::string> emitters_to_load{"fire", "burst", "spark", "bubbles"};

    this->load_emitters(emitters_to_load);
    this->load_bgs(renderer, bgs_to_load, window_surface);
    this->load_sprites(renderer, sprites_to_load, window_surface);
    this->load_tilesets(renderer, tilesets_to_load, window_surface);
    this->load_decorations(renderer, decorations_to_load, window_surface);
}

Image DataManager::get_image(SDL_Renderer *renderer, std::string full_path, SDL_Rect position, SDL_Surface *window_surface)
{

    //  A priorir on a pas le droit aux images plus grande que l'écran
    // TODO: A voir

    SDL_Surface *optimized_surface = NULL;
    SDL_Texture *texture_to_use = NULL;
    SDL_Surface *surface_to_use = IMG_Load(full_path.c_str());
    if (surface_to_use == NULL)
    {
        printf("Unable to load image %s! SDL_image Error: %s\n", full_path.c_str(), IMG_GetError());
    }
    // std::cout << "[surface_to_use]" << surface_to_use << std::endl;

    optimized_surface = SDL_ConvertSurface(surface_to_use, window_surface->format, 0);
    // std::cout << "[optimized_surface]" << optimized_surface << std::endl;

    if (optimized_surface == NULL)
    {
        printf("Unable to optimize image %s! SDL Error: %s\n", full_path.c_str(), SDL_GetError());
        texture_to_use = SDL_CreateTextureFromSurface(renderer, optimized_surface);
    }
    else
    {
        texture_to_use = SDL_CreateTextureFromSurface(renderer, surface_to_use);
    }

    SDL_QueryTexture(texture_to_use, NULL, NULL, &position.w, &position.h);

    SDL_FreeSurface(surface_to_use);
    SDL_FreeSurface(optimized_surface);

    Image image{texture_to_use, position};
    return image;
}

int DataManager::load_bgs(SDL_Renderer *renderer, std::vector<std::string> bgs_to_load, SDL_Surface *window_surface)
{
    for (auto bg_to_load : bgs_to_load)
    {
        std::string full_path = this->config.path + this->config.layers_path + bg_to_load;
        std::string extension = remove_extension(&bg_to_load);

        SDL_Rect position;
        position.x = 0;
        position.y = 0;

        Image image = this->get_image(renderer, full_path, position, window_surface);
        this->layers.insert({bg_to_load, image});
    }
    return 0;
}

int DataManager::load_sprites(SDL_Renderer *renderer, std::vector<std::string> sprites_to_load, SDL_Surface *window_surface)
{
    for (auto sprite_to_load : sprites_to_load)
    {
        std::string full_path = this->config.path + this->config.characters_path + sprite_to_load;
        std::string extension = remove_extension(&sprite_to_load);

        Json::Value config;
        std::string config_path = this->config.path + this->config.path_config + sprite_to_load + ".json";
        std::ifstream config_file(config_path, std::ifstream::binary);
        config_file >> config;

        SDL_Rect position;
        position.x = 0;
        position.y = 0;
        Image sheet_img = this->get_image(renderer, full_path, position, window_surface);

        Spritesheet *sprite_sheet = new Spritesheet(config, sheet_img);

        this->sprites.insert({sprite_to_load, sprite_sheet});
    }
    return 0;
}

int DataManager::load_decorations(SDL_Renderer *renderer, std::vector<std::string> decorations_to_load, SDL_Surface *window_surface)
{
    for (auto decoration_to_load : decorations_to_load)
    {
        std::string full_path = this->config.path + this->config.decorations_path + decoration_to_load;
        std::string extension = remove_extension(&decoration_to_load);
        Json::Value config;
        std::string config_path = this->config.path + this->config.path_config + decoration_to_load + ".json";
        std::ifstream config_file(config_path, std::ifstream::binary);
        config_file >> config;

        SDL_Rect position;
        position.x = 0;
        position.y = 0;
        Image sheet_img = this->get_image(renderer, full_path, position, window_surface);
        Spritesheet *sprite_sheet = new Spritesheet(config, sheet_img);
        Image sheet_img_black = this->get_image(renderer, full_path, position, window_surface);
        SDL_SetTextureColorMod(sheet_img_black.texture, 0, 0, 0);
        sprite_sheet->add_effect(sheet_img_black, "black");
        this->decorations.insert({decoration_to_load, sprite_sheet});
    }
    return 0;
}

int DataManager::load_tilesets(SDL_Renderer *renderer, std::vector<std::string> tilesets_to_load, SDL_Surface *window_surface)
{
    for (auto tileset_to_load : tilesets_to_load)
    {
        std::string full_path = this->config.path + this->config.tilesets_path + tileset_to_load;
        std::string extension = remove_extension(&tileset_to_load);

        Json::Value config;
        std::string config_path = this->config.path + this->config.path_config + tileset_to_load + ".json";
        std::ifstream config_file(config_path, std::ifstream::binary);
        config_file >> config;

        SDL_Rect position;
        position.x = 0;
        position.y = 0;
        Image sheet_img = this->get_image(renderer, full_path, position, window_surface);

        Spritesheet *sprite_sheet = new Spritesheet(config, sheet_img);

        this->tilesets.insert({tileset_to_load, sprite_sheet});
    }
    return 0;
}

int DataManager::load_level(std::string level_name, Json::Value &level_to_fill)
{
    std::cout << "[load_level]" << std::endl;
    const std::string extension = ".json";
    std::string level_full_path = this->config.path + this->config.maps_path + level_name + extension;
    std::ifstream level_file(level_full_path, std::ifstream::binary);
    level_file >> level_to_fill;
    return 0;
}

int DataManager::load_emitters(std::vector<std::string> emitters_to_load)
{
    std::cout << "[load_emitters]" << std::endl;
    const std::string extension = ".json";
    for (auto emitter : emitters_to_load)
    {
        std::string emitter_full_path = this->config.path + this->config.emmiters_path + emitter + extension;
        std::ifstream emitter_file(emitter_full_path, std::ifstream::binary);
        Json::Value emitter_to_fill;
        emitter_file >> emitter_to_fill;
        this->emitters.insert({emitter, emitter_to_fill});
    }
    return 0;
}

// Enlève l'extension du fichier et le retourne
std::string remove_extension(std::string *fullname)
{
    std::string to_work_string = *fullname;
    size_t lastindex = to_work_string.find_last_of(".");
    std::string rawname = to_work_string.substr(0, lastindex);
    std::string extension = to_work_string.substr(lastindex + 1, to_work_string.size() - 1);
    *fullname = rawname;
    return extension;
}
