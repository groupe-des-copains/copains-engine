#include <iostream>
#include "entity.hpp"

Entity::Entity(Spritesheet *sheet) : Solid(sheet)
{
    (void)sheet;
    this->coinf =  new Coinf(300,1);
    std::cout << "[create entity]" << std::endl;
}

int Entity::manage_event(std::set<SDL_Keycode> keys)
{
    (void)keys;
    return 0;
}

int Entity::update_position(SDL_Event e)
{
    (void)e;
    return 0;
}
