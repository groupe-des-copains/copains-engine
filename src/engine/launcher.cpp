#include <time.h> /* time */
#include "SDL2/SDL_image.h"
#include "launcher.hpp"

Launcher::Launcher(ConfigData config_data)
{

    this->resolution.width = config_data.screen_width;
    this->resolution.height = config_data.screen_height;

    this->init_graphics(config_data.game_name);
    SDL_Surface *window_surface = SDL_GetWindowSurface(this->window);
    this->data_manager = new DataManager(config_data, renderer, window_surface);
    this->reset_states();
}

int Launcher::init_graphics(std::string game_name)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        std::cout << "SDL could not be initialized!" << std::endl
                  << "SDL_Error: " << SDL_GetError() << std::endl;
        return 0;
    }

#if defined linux && SDL_VERSION_ATLEAST(2, 0, 8)
    // Disable compositor bypass
    if (!SDL_SetHint(SDL_HINT_VIDEO_X11_NET_WM_BYPASS_COMPOSITOR, "0"))
    {
        std::cout << "SDL can not disable compositor bypass!" << std::endl;
        return 0;
    }
#endif

    // Create window
    this->window = SDL_CreateWindow(game_name.c_str(),
                                    SDL_WINDOWPOS_UNDEFINED,
                                    SDL_WINDOWPOS_UNDEFINED,
                                    this->resolution.width, this->resolution.height,
                                    SDL_WINDOW_SHOWN);

    this->renderer = SDL_CreateRenderer(this->window, -1, SDL_RENDERER_ACCELERATED);
    SDL_SetRenderDrawBlendMode(this->renderer, SDL_BLENDMODE_BLEND);
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "2");
    if (!renderer)
    {
        std::cout << "Renderer could not be created!" << std::endl
                  << "SDL_Error: " << SDL_GetError() << std::endl;
    }
    return 0;
}

int Launcher::main_loop(void)
{
    bool quit = false;
    if (!window)
    {
        std::cout << "Window could not be created!" << std::endl
                  << "SDL_Error: " << SDL_GetError() << std::endl;
    }
    else
    {
        while (!quit)
        {

            SDL_Event e;
            int key;

            // Wait indefinitely for the next available event
            while (SDL_PollEvent(&e) != 0)
            {
                switch (e.type)
                {
                case SDL_KEYDOWN:
                    key = e.key.keysym.sym;
                    this->keys.insert(key);
                    break;
                case SDL_KEYUP:
                    key = e.key.keysym.sym;
                    this->keys.erase(key);
                    break;

                case SDL_QUIT:
                    quit = true;
                    break;
                }
            }
            this->get_active()->manage_key(this->keys);

            // User requests quit

            SDL_RenderClear(renderer);

            this->get_active()->draw(renderer, this->resolution);

            // std::cout << "w:" << this->resolution.width << std::endl;
            SDL_RenderPresent(renderer);
            // Clear screen
        }

        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
    }

    SDL_Quit();
    return 0;
}

State *Launcher::get_active(void)
{
    switch (State::state_code)
    {
    case States::NAV:
        return this->nav;

    case States::GAME:
        return this->game;

    case States::PAUSE:
        return this->pause;

    case States::END:
        return this->end;
    default:
        break;
    }
    return this->nav;
}

int Launcher::reset_states(void)
{
    this->nav = new Nav();
    this->end = new End();
    this->game = new Game(this->data_manager);
    this->pause = new Pause();
    State::set_state(States::NAV);

    return 0;
}
