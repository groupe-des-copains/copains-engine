#include <iostream>
#include "world.hpp"

World::World(DataManager *data_manager, LevelManager *level_manager)
{
    this->data_manager = data_manager;
    this->level_manager = level_manager; //TODO: voir si on le garde là
    std::cout << "[create world]" << std::endl;
    this->camera = new Camera(level_manager->get_rect_map_config(), {1280, 720});
}

int World::draw_background(SDL_Renderer *renderer, Layer layer)
{
    auto bg = layer.background;

    if (SDL_SetRenderDrawColor(renderer, bg.red, bg.green, bg.blue, bg.opacity) == -1)
    {
        std::cout << SDL_GetError() << std::endl;
    }
    else
    {
        SDL_Rect rectangle;
        rectangle.x = 0;
        rectangle.y = 0;
        rectangle.w = 1280;
        rectangle.h = 720;
        SDL_RenderFillRect(renderer, &rectangle);
    }

    return 0;
}

int World::draw(SDL_Renderer *renderer)
{
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_Rect rectangle;
    rectangle.x = 0;
    rectangle.y = 0;
    rectangle.w = 1280;
    rectangle.h = 720;

    SDL_RenderFillRect(renderer, &rectangle);
    Uint32 ticks = SDL_GetTicks();

    this->dt = this->frame_timer.read_sec();
    this->frame_timer.start();

    SDL_Rect rect_player = this->entities[0]->get_rect();
    rect_player.x = this->entities[0]->coinf->pos->pos_x;
    rect_player.y = this->entities[0]->coinf->pos->pos_y;
    this->camera->update_camera(rect_player);

    this->update_position();

    for (unsigned int i = 0; i < this->level_manager->max_perspective; i++)
    {
        if (this->level_manager->perspective_player == i)
        {
            for (auto entity : this->entities)
            {
                entity->animate(ticks);
                SDL_Rect source_rect = entity->get_rect();
                SDL_Rect pos_rect = source_rect;
                pos_rect.w = source_rect.w * entity->get_scale();
                pos_rect.h = source_rect.h * entity->get_scale();
                pos_rect.x = entity->coinf->pos->pos_x;
                pos_rect.y = entity->coinf->pos->pos_y;
                SDL_Rect applied_camera_rect = this->camera->apply(pos_rect);
                SDL_RenderCopy(renderer, entity->get_texture(), &source_rect, &applied_camera_rect);
            }
        }
        if (this->level_manager->perspective_tileset == i)
        {
            for (auto solid : this->solids)
            {
                SDL_Rect source_rect = solid->get_rect();
                SDL_Rect pos_rect = source_rect;

                pos_rect.x = solid->get_pos_x() * solid->get_scale();
                pos_rect.y = solid->get_pos_y() * solid->get_scale();

                SDL_Rect applied_camera_rect = this->camera->apply(pos_rect);

                applied_camera_rect.w *= solid->get_scale();
                applied_camera_rect.h *= solid->get_scale();

                SDL_RenderCopy(renderer, solid->get_texture(), &source_rect, &applied_camera_rect);
            }
        }
        if (this->layers.count(i))
        {
            auto layer = this->layers[i];
            for (auto deco : layer.decorations)
            {
                deco->animate(ticks);
                SDL_Rect source_rect = deco->get_rect();
                SDL_Rect pos_rect = source_rect;
                pos_rect.w = source_rect.w * deco->get_scale() * layer.scale;
                pos_rect.h = source_rect.h * deco->get_scale() * layer.scale;
                pos_rect.x = deco->get_pos_x();
                pos_rect.y = deco->get_pos_y();
                SDL_Rect applied_camera_rect = this->camera->apply_layer(pos_rect, layer.coeff_perspective);
                auto texture_to_use = deco->get_texture();
                if (layer.effect == "black")
                {
                    // TODO: voir pour préparer les effets et juste appeler ?
                    texture_to_use = deco->get_texture("black");
                }
                SDL_RenderCopyEx(renderer, texture_to_use, &source_rect, &applied_camera_rect, deco->get_rotation(), NULL, deco->get_flip());
            }
            this->draw_background(renderer, layer);
        }
    }
    this->particles_manager->update_emitters(renderer, this->dt);

    return 0;
}

int World::manage_event(std::set<SDL_Keycode> keys)
{
    for (auto entity : this->entities)
    {
        entity->manage_event(keys);
    }

    return 0;
}

int World::update_position()
{
    for (auto entity : this->entities)
    {
        entity->coinf->move();
    }
    return 0;
}

int World::add_layer(int id, Layer layer)
{
    this->layers.insert({id, layer});
    return 0;
}

int World::add_entity(Entity *entity)
{
    this->entities.push_back(entity);
    return 0;
}
int World::add_solid(Solid *solid)
{
    this->solids.push_back(solid);
    return 0;
}
