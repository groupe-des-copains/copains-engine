// #pragma once
#include "player.hpp"
#include <iostream>

Player::Player(Spritesheet *sheet) : Character(sheet)
{
    (void)sheet;
    std::cout << "[create player]" << std::endl;
}

int Player::manage_event(std::set<SDL_Keycode> keys)
{

    // std::cout << "[update position player]" << std::endl;


    if (keys.count(SDLK_LEFT))
    {
        if (this->coinf->running==false)
        {
            this->coinf->running=true;
        };             
        this->coinf->vit->dx-=1;
    };

    if (keys.count(SDLK_RIGHT))
    {
        if (this->coinf->running==false)
        {
            this->coinf->running=true;
        };
        this->coinf->vit->dx+=1;
    };

    if (keys.count(SDLK_UP))
    {
        this->coinf->vit->dy = 30;
    };

    return 0;

}