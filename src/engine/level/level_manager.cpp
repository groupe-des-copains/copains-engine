#include "level/level_manager.hpp"
#include <algorithm> // std::sort

LevelManager::LevelManager(DataManager *data_manager)
{
    std::cout << "[init level_manager]" << std::endl;
    this->data_manager = data_manager;
    Json::Value level_to_fill;
    this->data_manager->load_level("level_one", level_to_fill);
    std::cout << "load_level : " << level_to_fill["name"] << std::endl;
    this->load_map(level_to_fill["map"]);
    this->load_layers(level_to_fill["layers"]);
    this->max_perspective = level_to_fill["max_index_perspective"].asUInt();
    this->perspective_tileset = level_to_fill["perspective_tileset"].asUInt();
    this->perspective_player = level_to_fill["perspective_player"].asUInt();
    this->height_case = 128;
    this->width_case = 128;
}
SDL_Rect LevelManager::get_rect_map_config()
{
    SDL_Rect rect;
    rect.x = this->width_map;
    rect.y = this->height_map;
    rect.w = this->width_case;
    rect.h = this->height_case;
    return rect;
}

int LevelManager::load_map(Json::Value raw_map)
{
    int nb_sprites = 0;
    this->width_map = raw_map[0].size();
    this->height_map = raw_map.size();
    for (unsigned int j = 0; j < raw_map.size(); j++)
    {
        for (unsigned int i = 0; i < raw_map[j].size(); i++)
        {
            bool bloc_right = false;
            bool bloc_left = false;
            bool bloc_up = false;
            bool bloc_down = false;

            std::string name_sprite = "";
            if (raw_map[j][i] > 0)
            {
                if (i <= 0 || raw_map[j][i - 1] <= 0)
                {
                    name_sprite += "n";
                    bloc_left = true;
                }
                name_sprite += "left_";

                if (i >= raw_map[j].size() || raw_map[j][i + 1] <= 0)
                {
                    name_sprite += "n";
                    bloc_right = true;
                }
                name_sprite += "right_";

                if (j <= 0 || raw_map[j - 1][i] <= 0)
                {
                    name_sprite += "n";
                    bloc_up = true;
                }
                name_sprite += "up_";

                if (j + 1 >= raw_map.size() || raw_map[j + 1][i] <= 0)
                {
                    name_sprite += "n";
                    bloc_down = true;
                }
                name_sprite += "down_";

                if (bloc_left || bloc_up || !(i > 0 && j > 0 && raw_map[j - 1][i - 1] > 0))
                {
                    name_sprite += "n";
                }
                name_sprite += "up_left_";

                if (bloc_right || bloc_up || !(j > 0 && i < raw_map[j].size() && raw_map[j - 1][i + 1] > 0))
                {
                    name_sprite += "n";
                }
                name_sprite += "up_right_";

                if (bloc_left || bloc_down || !(i > 0 && j + 1 < raw_map.size() && raw_map[j + 1][i - 1] > 0))
                {
                    name_sprite += "n";
                }
                name_sprite += "down_left_";

                if (bloc_right || bloc_down || !(j + 1 < raw_map.size() && i < raw_map[j].size() && raw_map[j + 1][i + 1] > 0))
                {
                    name_sprite += "n";
                }
                name_sprite += "down_right";

                nb_sprites++;
                // TODO: voir pour les frames en double
                auto tile_to_add = new Tile(this->data_manager->tilesets["tileset"], i, j, name_sprite);
                this->solids.push_back(tile_to_add);
            }
        }
    }
    return 0;
}

int LevelManager::load_layers(Json::Value layers)
{
    for (auto layer : layers)
    {
        Layer layer_to_add;
        layer_to_add.coeff_perspective = layer["coeff_perspective"].asFloat();
        layer_to_add.id = layer["id"].asUInt();
        layer_to_add.effect = layer["effect"].asString();
        layer_to_add.scale = layer["scale"].asFloat();
        Background bg;
        bg.red = layer["background"]["red"].asInt();
        bg.green = layer["background"]["green"].asInt();
        bg.blue = layer["background"]["blue"].asInt();
        bg.opacity = layer["background"]["opacity"].asInt();
        layer_to_add.background = bg; //TODO: faire une struc propre
        std::vector<Decoration *> decorations_to_add;
        for (auto deco : layer["decorations"])
        {
            auto name = deco["name"].asString();
            auto x = deco["x"].asUInt();
            auto y = deco["y"].asInt();
            auto start_anim = deco["start_anim"].asInt();

            std::string etat = "none";
            if (deco["etat"].isString())
            {
                etat = deco["etat"].asString();
            }

            // auto state = deco["state"].asString(); // récup la bonne image en cas de format précis
            auto flip_horizontal = deco["flip_horizontal"].asBool();
            auto flip_vertical = deco["flip_vertical"].asBool();
            auto rotation = deco["rotation"].asInt();
            auto scale = deco["scale"].asFloat();
            // std::cout << "state: " << state << std::endl;
            auto deco_to_add = new Decoration(this->data_manager->decorations[name], etat, x, y, flip_horizontal, flip_vertical, rotation, scale);
            if (start_anim)
            {
                deco_to_add->set_anim(start_anim);
            }
            // if(etat)
            //     deco_to_add->etat = etat;

            // std::cout << "name : " << name << std::endl;
            // std::cout << "tick_anim : " << deco_to_add->tick_anim << std::endl;
            // std::cout << "etat : " << etat << std::endl;
            // std::cout << "etat : " << deco_to_add->etat << std::endl;

            // deco_to_add->etat = state; // récup la bonne image en cas de format précis
            // Voir pour carrément une range anim ?
            decorations_to_add.push_back(deco_to_add);
        }
        layer_to_add.decorations = decorations_to_add;
        this->layers.push_back(layer_to_add);
    }
    return 0;
}

std::vector<Layer> LevelManager::get_layers()
{
    //TODO: vérifier le tri avant de return
    return this->layers;
}
