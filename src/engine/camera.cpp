#include "camera.hpp"
#include <iostream>
#include <algorithm>

Camera::Camera(SDL_Rect rect_map_config, screen_resolution screen_config)
{
    std::cout << "[create camera]" << std::endl;
    this->width_map = rect_map_config.x * rect_map_config.w;
    this->height_map = rect_map_config.y * rect_map_config.h;
    this->rect_map_config = rect_map_config;
    this->state.x = 0;
    this->state.y = 0;
    this->state.w = this->width_map;
    this->state.h = this->height_map;

    this->screen_config = screen_config;
}

SDL_Rect Camera::apply(SDL_Rect target_rect)
{
    SDL_Rect rect = target_rect;
    rect.x += this->state.x;
    rect.y += this->state.y;
    return rect;
}

SDL_Rect Camera::apply_layer(SDL_Rect target_rect, float coeff_perspective)
{
    SDL_Rect rect = target_rect;
    rect.x += this->state.x*coeff_perspective;
    rect.y += this->state.y*coeff_perspective;
    return rect;
}

// def apply_layer(self, target):
//     index = target.index
//     index = math.floor(math.sqrt(index)*index)
//     to_add = target.index_layer *1280
//     x = (self.state.topleft[0]+target.x)/(index)+to_add
//     y = self.state.topleft[1]/(index)

//     return target.rect.move(x, y)

void Camera::update_camera(SDL_Rect target_rect)
{
    this->state = this->camera_func(target_rect);
}

SDL_Rect Camera::camera_func(SDL_Rect target_rect)
{

    // TODO: voir pour centrer au mileur de l'image et non son state-left

    int max_x = this->state.w - this->screen_config.width;
    int max_y = this->state.h - this->screen_config.height;

    int x = target_rect.x;
    int y = target_rect.y;

    x = -x + (this->screen_config.width/2);
    y = -y + (this->screen_config.height/2);

    x = std::min(0, x);          // stop scroll à gauche
    y = std::min(0, y);          // stop scroll en haut

    x = std::max(-(max_x), x); // stop scroll à droite
    y = std::max(-(max_y), y); // stop scroll en bas


    SDL_Rect to_return;
    to_return.x = x;
    to_return.y = y;
    to_return.w = this->state.w;
    to_return.h = this->state.h;

    return to_return;
}
