// #include <time.h> /* time */
// #include "SDL2/SDL_image.h"
#include "state.hpp"

States State::state_code = States::UNDEFINED;

State::State()
{
    std::cout << "[create state]" << std::endl;
}

int State::manage_event(std::set<SDL_Keycode> e)
{
    std::cout << "[manage_event]" << std::endl;
    // std::cout << e.type << std::endl;
    (void)e;

    // switch (e.type)
    // {
    // // case SDL_KEYUP:
    // //     std::cout << "[keyup]" << std::endl;
    // //     break;
    // case SDL_KEYDOWN:
    //     this->manage_key(e);
    //     break;
    // }

    return 0;
}

int State::manage_key(std::set<SDL_Keycode> keys)
{   
    (void)keys;


    // switch (e.key.keysym.sym) // a voir pour ce type
    // {
    // case SDLK_RIGHT:
    //     std::cout << "[droite]" << std::endl;
    //     State::set_state(States::UNDEFINED);
    //     break;
    // }
    return 0;
}

void State::set_state(States state)
{
    State::state_code = state;
}


int State::draw(SDL_Renderer *renderer, screen_resolution resolution)
{
    (void)renderer;
    (void)resolution;
    std::cout << "draw in state" << std::endl;
    return 0;
}
