#include <algorithm>
#include <iostream>
#include <SDL.h>

#include "../engine/launcher.hpp"
#include "types/config_data.hpp"

int main(int argc, char *argv[])
{
    // Unused argc, argv
    (void)argc;
    (void)argv;

    ConfigData config_data{
        "test jeu",
        1280,
        720,
        "data/",
        "audio/",
        "assets/layers/",
        "assets/spritesets/characters/",
        "assets/tilesets/",
        "obstacles/",
        "levels/",
        "config_sprites/",
        "config_particles/",
        "assets/spritesets/decorations/"};
        
    auto launcher = new Launcher(config_data); 
    launcher->main_loop();

    return 0;
}
