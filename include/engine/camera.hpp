#pragma once
#include <iostream>
#include <SDL.h>
#include "types/screen_resolution.hpp"
#include "ressources/sprite.hpp"

class Camera
{
public:
    Camera(SDL_Rect rect_map_config, screen_resolution screen_config);
    void update_camera(SDL_Rect target_rect);
    SDL_Rect apply(SDL_Rect target_rect);
    SDL_Rect apply_layer(SDL_Rect target_rect, float coeff_perspective);
    SDL_Rect camera_func(SDL_Rect target_rect);

private:
    SDL_Rect rect_map_config;
    SDL_Rect state;
    screen_resolution screen_config;
    unsigned int width_map;
    unsigned int height_map;
};
