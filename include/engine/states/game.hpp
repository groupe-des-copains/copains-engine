#pragma once
#include <SDL.h>
#include <iostream>
#include "../../engine/state.hpp"
#include "../../engine/ressources/tile.hpp"
#include "../../engine/level/level_manager.hpp"
#include "../../engine/particles/particles_manager.hpp"
#include "world.hpp"
#include "../../engine/data_manager.hpp"
#include "player.hpp"
#include <vector>
#include <set>

class Game : public State
{
public:
    Game();
    int manage_event(std::set<SDL_Keycode> e);
    int manage_key(std::set<SDL_Keycode>);
    int draw(SDL_Renderer *renderer, screen_resolution screen_res);
    Game(DataManager *data_manager);
    int gen_entities();

protected:
    Player *player;
    World *world;
    DataManager *data_manager;
    ParticlesManager *particles_manager;
    LevelManager *level_manager;
};