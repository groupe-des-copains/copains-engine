#pragma once
#include <SDL.h>
#include <iostream>
#include "../../engine/state.hpp"

class End : public State
{
public:
    int manage_event(SDL_Event e);
    int manage_key(SDL_Event e);
    int draw(SDL_Renderer *renderer, screen_resolution screen_res);
    // End();

protected:

};