#pragma once

enum class States{
    UNDEFINED,
    NAV,
    GAME,
    PAUSE,
    END
};