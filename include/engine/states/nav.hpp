#pragma once
#include <SDL.h>
#include <iostream>
#include "../../engine/hud/nav_state.hpp"

class Nav : public NavState
{
public:
    Nav();
    int manage_event(std::set<SDL_Keycode>);
    int manage_key(std::set<SDL_Keycode>);
    int init_texts() override;
    int draw(SDL_Renderer *renderer, screen_resolution screen_res);

protected:
};