#pragma once

enum class TextPosition: int
{
    NONE,
    CENTER,
    LEFT,
    RIGHT,
    NEXT,
    TOP,
    MID,
    BOTTOM,
    CUSTOM
};