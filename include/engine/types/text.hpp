#pragma once
#include <SDL.h>
#include <SDL_ttf.h>
#include "text_position.hpp"

struct Text
{
    const char* text;
    TTF_Font *font;
    SDL_Color color;
    TextPosition x;
    TextPosition y;
    int custom_x;
    int custom_y;
};