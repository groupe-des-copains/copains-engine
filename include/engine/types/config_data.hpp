#pragma once

struct ConfigData
{
    std::string game_name;
    int screen_width;
    int screen_height;
    std::string path;
    std::string sound_path;
    std::string layers_path;
    std::string characters_path;
    std::string tilesets_path;
    std::string obstacles_path;
    std::string maps_path;
    std::string path_config;
    std::string emmiters_path;
    std::string decorations_path;
};