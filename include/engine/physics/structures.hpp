#pragma once

struct POS {
    float pos_x;
    float pos_y;
};

struct VIT {
    float dx;
    float dy;
    float max_dx;
};

struct INE {
    float frottement;
};