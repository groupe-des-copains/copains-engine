#pragma once
#include <tuple>
#include "collision.hpp"
#include "structures.hpp"

class Coinf
{
public:
    INE *ine;
    POS *pos;
    VIT *vit;
    bool running;
    Coinf(float, float);

    int move();
    // std::tuple<float,float> pos(void);
    // std::tuple<float,float> speed(void);
};