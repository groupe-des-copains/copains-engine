#pragma once
#include <iostream>
#include "gravite.hpp"
#include "inertie.hpp"
#include <math.h> 
#include <chrono>
#include <thread>


int collision_check();
int Get_Position(struct VIT *, struct POS *, INE *);
int compute_hitboxes();
