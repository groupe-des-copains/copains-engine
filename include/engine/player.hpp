#pragma once
#include "character.hpp"
#include <math.h>
#include <set>
class Player : public Character
{
public:
    Player(Spritesheet *sheet);
    int manage_event(std::set<SDL_Keycode>);
};
