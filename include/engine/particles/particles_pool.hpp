#pragma once

#include <iostream>
#include <assert.h>
#include <SDL.h>
#include "emitter.hpp"
#include "particle.hpp"
#include "../types/point.hpp"
#include <new>        

class Emitter;

enum ParticleState
{
    PARTICLE_STATE_NOT_DEF,
    PARTICLE_ALIVE_DRAWN,
    PARTICLE_ALIVE_NOT_DRAWN,
    PARTICLE_DEAD
};

class ParticlesPool
{
private:
    int pool_size = 0;
    Particle *first_available;
    Particle *particle_array = nullptr;

public:
    ParticlesPool(Emitter *emitter);
    virtual ~ParticlesPool();

    //Generates a new particle each time it's called
    int generate(fPoint pos,
                 float start_speed,
                 float end_speed,
                 float angle,
                 double rot_speed,
                 float start_size,
                 float end_size,
                 unsigned int life,
                 Spritesheet *sheet,
                 SDL_Color start_color,
                 SDL_Color end_color,
                 SDL_BlendMode blend_mode,
                 bool vortex_sensitive,
                 std::string particle_texture);

    // Update (move and draw) particles in the pool. If there are no particles alive returns false
    ParticleState update(float dt, SDL_Renderer *renderer);
};
