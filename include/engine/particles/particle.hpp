#pragma once
#include <iostream>
#include <SDL.h>
#include <vector>
#include "ressources/sprite.hpp"
#include "ressources/spritesheet.hpp"
#include "../types/point.hpp"
#define MIN_LIFE_TO_INTERPOLATE 15

class Particle 
{
public:
    Particle();
    void init(fPoint pos,
              float start_speed,
              float end_speed,
              float angle,
              double rot_speed,
              float start_size,
              float end_size,
              unsigned int life,
              Spritesheet *sheet,
              SDL_Color start_color,
              SDL_Color end_color,
              SDL_BlendMode blend_mode,
              bool vortex_sensitive,
              std::string particle_texture);

    int update(float dt);
    bool is_alive(void);
    Particle *get_next(void);
    void set_next(Particle *next);
    bool draw(SDL_Renderer *renderer);
    SDL_Color rgb_interpolation(SDL_Color start_color, float time_step, SDL_Color end_color);
    float interpolate_between_range(float min, float time_step, float max);
    void add_vortex(fPoint pos, float speed, float scale);
    void calculate_particle_pos(float dt);

protected:
    Spritesheet *sheet;
    /*  This is the only variable we care about no matter if
	   the particle is alive or dead */
    unsigned int life = 0;
    struct Vortex
    {
        fPoint pos = {0.0f, 0.0f};
        float speed = 0.0f;
        float scale = 0.0f;
    } vortex;

    union ParticleInfo
    {
        /* This struct holds the state of the particle when 
		   it's being update (it's still alive).*/
        struct ParticleState
        {
            unsigned int start_life;
            fPoint pos;
            fPoint start_vel;
            fPoint end_vel;
            fPoint current_vel;
            float current_size, start_size, end_size;
            float age_ratio;
            float angle;
            double start_rot_speed;
            double current_rot_speed;
            SDL_Rect rect;
            SDL_Rect rect_size;
            SDL_Color start_color;
            SDL_Color end_color;
            SDL_BlendMode blend_mode;
            bool vortex_sensitive;
            float t; // step

            ParticleState() {}

        } live;

        /* If the particle is dead, then the 'next' member comes 
		   into play and the struct it's not used. This pointer
		   called 'next' holds a pointer to the next available 
		   particle after this one. */
        Particle *next;

        ParticleInfo() {}
    } state;
};
