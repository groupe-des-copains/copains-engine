#pragma once
#include <iostream>
#include <SDL.h>
#include <vector>
#include "../data_manager.hpp"
#include "emitter.hpp"
#include <map>
#include <iterator>
#include "../types/point_defs.hpp"

class ParticlesManager
{
public:
    ParticlesManager(DataManager *data_manager);
    int create_emitter(iPoint pos, std::string particles_effect, std::string name);
    int update_emitters(SDL_Renderer *renderer, float dt);

protected:
    DataManager *data_manager;
    std::map<std::string, Emitter*> emitters;
    
};
