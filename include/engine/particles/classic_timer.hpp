#pragma once
#include "../types/scalar_types.hpp"

class ClassicTimer
{
public:
	ClassicTimer();

	void start();
	float read_sec() const;
	uint32 read() const;

private:
	uint32 started_at;
};
