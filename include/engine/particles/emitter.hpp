#pragma once
#include <iostream>
#include <SDL.h>
#include <json/value.h>
#include <json/json.h>
#include "particles_pool.hpp"
#include "emission_timer.hpp"
#include "../types/point.hpp"
#include "../ressources/spritesheet.hpp"

class ParticlesPool;

class Emitter // Salut victor
{
public:
    struct Pos //TODO: voir ce qu'on fait de ça
    {
        float x;
        float y;
    };
    struct IntRange
    {
        int max;
        int min;
    };
    // TODO : voir ce qu'on fait de ces strucs ptet des point en mode classe automatique
    struct FloatRange
    {
        float min;
        float max;
    };

    Emitter(float pos_x, float pos_y, Json::Value &emitter_data, Spritesheet *sheet);
    virtual ~Emitter();

    void update();
    bool draw(float dt, SDL_Renderer *renderer);

    // Generates random number between given range
    float range_random_num(Emitter::FloatRange range);
    int get_color_rand(int color, int variance);

    // Starts emission specified by timer, if not emission time is infinite
    void start_emission(double timer);

    // Stops emission specified by timer, if not emission stop time is infinite
    void stop_emission(double timer);

    // Emitter move methods
    void move_emitter(fPoint pos);
    fPoint get_emitter_pos() const;

    void set_vortex_sensitive(bool sensitive);
    
    FloatRange get_range (Json::Value data_to_transform);

    void init_colors(Json::Value start_color, Json::Value end_color, Json::Value color_variance);

    unsigned int get_pool_size();

    bool have_to_destroy();

protected:
    unsigned int emission_rate;

    unsigned int pool_size = 0;

    unsigned int max_particles_per_frame = 0;
    unsigned int max_particle_life = 0;

    unsigned int emit_number = 0;
    unsigned int emit_variance = 0;

    fPoint pos = {0, 0};

    float start_size = 0, end_size = 0;

    float start_speed = 0, end_speed = 0;
    double rot_speed = 0;

    FloatRange angle_range = {0, 360};

    ParticlesPool *my_pool = nullptr;

    double stop_time = 0;
    double emission_time = 0;
    double life_time = -1;

    EmissionTimer stop_timer;
    EmissionTimer emission_timer;
    EmissionTimer life_timer;

    bool active = false;

    SDL_Color start_color = {0, 0, 0, 0};
    SDL_Color end_color = {0, 0, 0, 0};
    SDL_BlendMode blend_mode = SDL_BlendMode::SDL_BLENDMODE_NONE;
    float time_step = 0;

    FloatRange rot_speed_rand = {0.0, 0.0};
    FloatRange start_speed_rand = {0.0, 0.0};
    FloatRange end_speed_rand = {0.0, 0.0};
    FloatRange emit_variance_rand = {0.0, 0.0};
    FloatRange life_rand = {0.0, 0.0};
    FloatRange start_size_rand = {0.0, 0.0};
    FloatRange end_size_rand = {0.0, 0.0};

    bool vortex_sensitive = false;

    Spritesheet *sheet;
    std::string particle_texture;

    bool to_destroy = false;
};