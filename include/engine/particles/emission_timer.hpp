#pragma once
#include "../types/scalar_types.hpp"
class EmissionTimer
{
public:

	EmissionTimer();

	void start();
	double read_ms() const;
	unsigned int read_ticks() const;

private:
	uint64 started_at;
	static uint64 frequency;

};
