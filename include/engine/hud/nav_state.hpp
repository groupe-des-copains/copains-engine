#pragma once

#include <SDL.h>
#include <SDL_ttf.h>
#include "../state.hpp"
#include "../types/text.hpp"
#include "data_manager.hpp"

class NavState : public State
{
public:
    // NavState(DataManager data_manager, screen_resolution screen_res, States name);
    NavState(States name);
    ~NavState(void);

protected:
    // int manage_event(SDL_Event e) override;
    // int manage_key(SDL_Event e) override;
    void draw_bg(SDL_Renderer *renderer, SDL_Color alpha_bg);
    virtual int init_texts();
    void draw_text(SDL_Renderer *renderer, Text text, screen_resolution screen_res);
    int draw(SDL_Renderer *renderer, screen_resolution screen_res) override;
    SDL_Point get_texture_size(SDL_Texture *texture);
    SDL_Color alpha_bg;
    SDL_Color text_color;
    TTF_Font *font;
    std::vector<Text> texts;
};