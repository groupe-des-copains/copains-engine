#pragma once
#include <iostream>
#include <SDL.h>
#include "spritesheet.hpp"

class Sprite
{
public:
    Sprite(Spritesheet *sheet);
    int animate(Uint32 ticks);
    SDL_Rect get_rect();
    float get_scale();
    // TODO: passer sur une seule variable position
    int get_pos_x();
    int get_pos_y();
    SDL_Texture *get_texture(std::string effect = "");

protected:
    Spritesheet *sheet;
    SDL_Rect rect;
    std::string etat;
    unsigned int tick_anim;
    float scale;
    int pos_x;
    int pos_y;

    std::vector<std::string> etats;
};
