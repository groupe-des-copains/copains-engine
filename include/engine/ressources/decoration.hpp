#pragma once
#include <iostream>
#include <SDL.h>
#include <vector>
#include "ressources/sprite.hpp"

class Decoration : public Sprite
{
public:
    Decoration(Spritesheet *sheet, std::string etat, int x, int y, bool flip_horizontal, bool flip_vertical, int rotation, float scale);
    float get_scale();
    int get_rotation();
    SDL_RendererFlip get_flip();
    void set_anim(unsigned int start_anim);

protected:
    SDL_RendererFlip flip;
    int rotation;
    float scale_into_layer;

};
