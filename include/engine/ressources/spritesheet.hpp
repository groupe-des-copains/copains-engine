#pragma once
#include <iostream>
#include <map>
#include <vector>
#include <SDL.h>
#include <json/value.h>
#include <json/json.h>

struct Image
{
    SDL_Texture *texture;
    SDL_Rect rect;
};

class Spritesheet
{
public:
    Spritesheet(Json::Value config, Image sheet_img);
    int load();
    int load_simple();
    int load_particular();
    int add_effect(Image sheet_img, std::string key);
    int set_color_alpha_mode(SDL_Color color);
    int set_blend_mode(SDL_BlendMode blend_mode);
    std::vector<SDL_Rect> get_animations(std::string texture);
    SDL_Texture *get_texture();
    SDL_Texture *get_texture_effect(std::string effect_name);
    std::string get_first_etat();
    float get_scale();

protected:
    std::map<std::string, std::vector<SDL_Rect>> animations;
    std::map<std::string, Image> effects;
    std::vector<std::string> etats;
    Image sheet_img;
    Json::Value config;
    std::vector<std::string> hitboxes;
};