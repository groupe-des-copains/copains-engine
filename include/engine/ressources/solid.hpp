#pragma once
#include <iostream>
#include <SDL.h>
#include <vector>
#include "ressources/sprite.hpp"


class Solid: public Sprite
{
public:
    Solid(Spritesheet *sheet);

protected:
    float constante_gravite;

};
