#pragma once
#include <SDL.h>
#include <iostream>
#include "states/states.hpp"
#include "types/screen_resolution.hpp"
#include <set>


class State
{
public:
    virtual int manage_event(std::set<SDL_Keycode> e);
    virtual int manage_key(std::set<SDL_Keycode>);
    virtual int draw(SDL_Renderer *renderer, screen_resolution resolution);
    States static state_code;
    static void set_state(States state);
    State();

protected:

};
