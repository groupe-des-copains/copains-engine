#pragma once
#include <SDL.h>
#include <iostream>
#include "state.hpp"
#include "types/screen_resolution.hpp"
#include "types/config_data.hpp"
#include "states/end.hpp"
#include "states/nav.hpp"
#include "states/game.hpp"
#include "states/pause.hpp"
#include "states/states.hpp"
#include "../engine/data_manager.hpp"
#include <set>

class Launcher
{
public:
    int main_loop(void);
    State *get_active(void);
    int reset_states(void);
    int init_graphics(std::string game_name);

    Launcher(ConfigData config_data);
    // ~Launcher(void);

protected:
    SDL_Window *window;
    SDL_Renderer *renderer;
    std::set<SDL_Keycode> keys;

    screen_resolution resolution;

    // Pour l'instant on va mettre les states ici en vrac on verra plus tard
    Nav *nav;
    End *end;
    Game *game;
    Pause *pause;
    DataManager *data_manager;
};
