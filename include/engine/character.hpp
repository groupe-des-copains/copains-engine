#pragma once
#include <iostream>
#include <SDL.h>
#include "entity.hpp"

class Character : public Entity
{
public:
    Character(Spritesheet *);

public:
    bool is_on_ground;
    bool is_collide;
    std::string etats[2];
};
