#pragma once
#include <iostream>
#include <SDL.h>
#include "data_manager.hpp"
#include <vector>
#include "ressources/solid.hpp"
#include "ressources/tile.hpp"
#include "ressources/decoration.hpp"

struct Background
{
    int red;
    int green;
    int blue;
    int opacity;
};

struct Layer
{
    unsigned int id;
    float coeff_perspective;
    std::string effect; // TODO: avoir un enum ici
    float scale;      
    std::vector<Decoration *> decorations;
    Background background;
};

class LevelManager
{
public:
    LevelManager(DataManager *data_manager);
    std::vector<Solid *> solids;
    std::vector<Layer> get_layers();
    SDL_Rect get_rect_map_config();
    unsigned int width_map;
    unsigned int height_map;
    unsigned int width_case;
    unsigned int height_case;
    unsigned int max_perspective;
    unsigned int perspective_tileset;
    unsigned int perspective_player;
    //TODO: passer en protected

protected:
    DataManager *data_manager;
    std::vector<Layer> layers;
    int load_map(Json::Value raw_map);
    int load_layers(Json::Value layers);
};
