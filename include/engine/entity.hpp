#pragma once
#include <iostream>
#include <SDL.h>
#include <vector>
#include "ressources/solid.hpp"
#include "physics/coinf.hpp"
#include <set>

class Entity : public Solid
{
public:
    Entity(Spritesheet *sheet);

    virtual int update_position(SDL_Event);
    virtual int manage_event(std::set<SDL_Keycode>);
    Coinf *coinf; //TODO: remplacer, à redesigner


protected:
    bool is_jump;
};
