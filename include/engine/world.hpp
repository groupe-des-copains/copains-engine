#pragma once
#include <iostream>
#include <SDL.h>
#include "data_manager.hpp"
#include <vector>
#include "entity.hpp"
#include "camera.hpp"
#include "../engine/level/level_manager.hpp"
#include "../engine/particles/particles_manager.hpp"
#include "../engine/particles/classic_timer.hpp"
#include "player.hpp"
#include "ressources/solid.hpp"
#include <set>

class World
{
public:
    World(DataManager *data_manager, LevelManager *level_manager);
    int draw(SDL_Renderer *renderer);
    int draw_background(SDL_Renderer *renderer, Layer layer);
    int manage_event(std::set<SDL_Keycode>);

    int draw_player(SDL_Renderer *renderer);
    int manage_event();
    int update_position();
    int manage_collision();
    int routine();
    int animate_entities();
    int add_layer(int id, Layer layer);
    int add_entity(Entity *entity);
    int add_solid(Solid *solid);

    // TODO passer en protected
    ParticlesManager *particles_manager; // singleton de fonction

protected:
    DataManager *data_manager;
    LevelManager *level_manager;
    ClassicTimer frame_timer;
    float dt;
    Camera *camera;
    bool have_emitter;
    std::vector<Entity *> entities;
    std::vector<Solid *> solids;
    std::map<int, Layer> layers;
};
