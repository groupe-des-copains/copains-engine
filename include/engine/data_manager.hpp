#pragma once
#include <fstream>
#include <iostream>
#include <map>
#include <SDL.h>
#include <SDL_image.h>
#include <vector>

#include "ressources/spritesheet.hpp"
#include "types/config_data.hpp"


class DataManager
{
public:
    DataManager(ConfigData config, SDL_Renderer *renderer, SDL_Surface *window_surface);

    // TODO passer en protected ou redesigner c'est un singleton ça parait logique pour l'instant d'avoir des maps
    std::map<std::string, Image> layers;
    std::map<std::string, Spritesheet*> sprites;
    std::map<std::string, Spritesheet*> decorations;
    std::map<std::string, Spritesheet*> tilesets;
    std::map<std::string, Json::Value> emitters;
    int load_level(std::string level_name, Json::Value &level_to_fill);


protected:
    int load_bgs(SDL_Renderer *renderer, std::vector<std::string> bgs_to_load, SDL_Surface *window_surface);
    int load_sprites(SDL_Renderer *renderer, std::vector<std::string> sprites_to_load, SDL_Surface *window_surface);
    int load_tilesets(SDL_Renderer *renderer, std::vector<std::string> tilesets_to_load, SDL_Surface *window_surface);
    int load_decorations(SDL_Renderer *renderer, std::vector<std::string> decorations_to_load, SDL_Surface *window_surface);
    int load_emitters(std::vector<std::string> emitters_to_load);
    Image get_image(SDL_Renderer *renderer, std::string full_path, SDL_Rect position, SDL_Surface *window_surface);
    ConfigData config;
};